import {createRouter, createWebHistory} from "vue-router/dist/vue-router";

import Cart from "@/components/cart";
import Catalog from "@/components/catalog";
import MainWrapper from "@/components/main-wrapper";


const routes = [
     {
         path: '/',
         name: 'catalog',
         component: Catalog
     },
     {
         path: '/cart',
         name: 'cart',
         component: Cart,
         props: true
     }
]

const router = createRouter({
    routes,
    history: createWebHistory(process.env.BASE_URL)
})

export default router