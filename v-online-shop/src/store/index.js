import Vuex from "vuex"
import stateCatalog from "@/store/modules/state-catalog-item";
import stateCartItem from "@/store/modules/state-cart-item";


export default new Vuex.Store({
    modules: {
        stateCatalog,
        stateCartItem,
    }
})